#!/bin/bash

EU_COUNTRIES=./data/EU27_list.csv

#STEP1
#Run Random Forest to predict nights spent for each available country
tail -n+2 $EU_COUNTRIES | while read LINE; do
    Rscript ./google_tourism_rf.R --country "$(echo $LINE| cut -d '"' -f 4)"
done

#STEP2a
#Generate mobility indicator correlation matrices (plot in img folder and tables)
Rscript ./nights_spent_mob_correlation.R

#STEP2b
#Generate the plots into img folder
Rscript ./plots.R

#STEP3
#generate tables
Rscript ./tables.R

