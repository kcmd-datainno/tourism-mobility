# Repository to replicate the results of the manuscript "Nowcasting tourist nights spent using innovative human mobility data"

Public repository of the R scripts and public data necessary to replicate results of : Minora U, Iacus SM, Batista e Silva F, Sermi F, Spyratos S (2023) Nowcasting tourist nights spent using innovative human mobility data. PLoS ONE 18(10): e0287063. https://doi.org/10.1371/journal.pone.0287063

To replicate the results, just run the [`run_all_countries.sh`](run_all_countries.sh) from a terminal [^*].

The [`img`](img) directory will get populated with the output.

[^*]: Tested on a Linux Mint 20 machine.